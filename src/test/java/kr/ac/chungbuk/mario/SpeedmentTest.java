package kr.ac.chungbuk.mario;

import org.junit.Test;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.speedment.core.db.model.ProjectManager;

import kr.ac.chungbuk.mario.imdg.SpeedmentHazelcastConfig;
import kr.ac.chungbuk.mario.imdg.TreeBuilder;

public class SpeedmentTest {

	@Test
	public void startup() {
		Config config = new Config();
		SpeedmentHazelcastConfig.addTo(config);
		HazelcastInstance instance = Hazelcast.newHazelcastInstance(config);
		ProjectManager.getInstance().putProperty(HazelcastInstance.class, instance);

		new TreeBuilder().build();
		ProjectManager.getInstance().init();
		try {
			Thread.sleep(10000000);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
