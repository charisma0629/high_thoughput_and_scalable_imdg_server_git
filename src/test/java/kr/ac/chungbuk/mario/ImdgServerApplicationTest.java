package kr.ac.chungbuk.mario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.hazelcast.core.HazelcastInstance;

@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
public class ImdgServerApplicationTest implements CommandLineRunner {

	@Autowired
	HazelcastInstance instance;

	public static void main(String[] args) {
		SpringApplication.run(ImdgServerApplicationTest.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		System.out.println(instance.toString());

	}
}
