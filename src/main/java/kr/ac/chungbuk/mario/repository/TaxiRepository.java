package kr.ac.chungbuk.mario.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import kr.ac.chungbuk.mario.dbms.Taxi;

@Transactional
@Repository
public class TaxiRepository {
	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;

	private static final RowMapper<kr.ac.chungbuk.mario.dbms.Taxi> taxiRowMapper = (rs, i) -> {
		return new Taxi(rs.getInt("taxi_id"), rs.getString("driver_name"), rs.getInt("status"), rs.getInt("company_id"), rs.getInt("city_id"), rs.getDouble("current_location_x"), rs.getDouble("current_location_y"));
	};
	
	public List<Taxi> findAll() {
		List<Taxi> list = jdbcTemplate.query("SELECT taxi_id, driver_name, status,company_id,city_id,current_location_x,current_location_y  FROM taxi", taxiRowMapper);
		return list;
	}

	public List<kr.ac.chungbuk.mario.dbms.Taxi> findTaxiNear2KmNaive(double x1, double y1) {
		SqlParameterSource param = new MapSqlParameterSource().addValue("x1", x1).addValue("y1", y1);
		// SELECT *, slc(36.0,current_location_y,127.0,current_location_x) as distance FROM mario_taxi.taxi where slc(36.0,current_location_y,127.0,current_location_x) <= 7000 ;
		List<Taxi> list = jdbcTemplate.query("SELECT taxi_id, driver_name, status, company_id, city_id, current_location_x, current_location_y FROM mario_taxi.taxi WHERE slc(:y1 ,:x1 ,current_location_y,current_location_x) <= 2000", param, taxiRowMapper);
		return list;
	}

	public List<kr.ac.chungbuk.mario.dbms.Taxi> findTaxiNear2KmSquare(double x1, double y1, double x2, double y2) {
		SqlParameterSource param = new MapSqlParameterSource().addValue("x1", x1).addValue("x2", x2).addValue("y1", y1).addValue("y2", y2);
		List<Taxi> list = jdbcTemplate.query("SELECT taxi_id, driver_name, status, company_id, city_id, current_location_x, current_location_y FROM mario_taxi.taxi WHERE current_location_x BETWEEN :x1 AND :x2 AND current_location_y BETWEEN :y1 AND :y2", param, taxiRowMapper);
		return list;
	}

	public List<kr.ac.chungbuk.mario.dbms.Taxi> findTaxiNear4Km(double x1, double y1, double x2, double y2) {
		SqlParameterSource param = new MapSqlParameterSource().addValue("x1", x1).addValue("x2", x2).addValue("y1", y1).addValue("y2", y2);
		List<Taxi> list = jdbcTemplate.query("SELECT taxi_id, driver_name, status, company_id, city_id, current_location_x, current_location_y FROM mario_taxi.taxi WHERE current_location_x BETWEEN :x1 AND :x2 AND current_location_y BETWEEN :y1 AND :y2", param, taxiRowMapper);
		return list;
	}

	public List<kr.ac.chungbuk.mario.dbms.Taxi> findTaxiNear2KmSquareGCD(double x1, double y1, double x2, double y2, double x0, double y0) {
		SqlParameterSource param = new MapSqlParameterSource().addValue("x1", x1).addValue("x2", x2).addValue("y1", y1).addValue("y2", y2).addValue("x0", x0).addValue("y0", y0);
		List<Taxi> list = jdbcTemplate.query("select taxi_id, driver_name, status, company_id, city_id, current_location_x, current_location_y from ( SELECT taxi_id, driver_name, status, company_id, city_id, current_location_x, current_location_y FROM mario_taxi.taxi WHERE current_location_x BETWEEN :x1 AND :x2 AND current_location_y BETWEEN :y1 AND :y2) as filtered_table WHERE slc(:y0,:x0,filtered_table.current_location_y,filtered_table.current_location_x) <= 2.0 ", param, taxiRowMapper);
		return list;
	}
}
