package kr.ac.chungbuk.mario.dbms;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "taxi")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Taxi {
	@Id
	@GeneratedValue
	@Column(name = "taxi_id")
	private Integer taxiId; // Field 0 ( 0, 1) : INT(10) NOT NULL AUTO_INCREMENT
	/**
	 * This field corresponds to the database column definition VARCHAR(255).
	 * 
	 * @Serial VARCHAR(255); This field was mapped using the type mapping STRING_MAPPING (java.lang.String, [B)
	 */
	@Column(name = "driver_name",nullable = false)
	private String driverName; // Field 1 ( 0, 2) : VARCHAR(255)
	/**
	 * This field corresponds to the database column definition INT(10).
	 * 
	 * @Serial INT(10); This field was mapped using the type mapping INTEGER_MAPPING (java.lang.Integer, int)
	 */
	@Column(name = "status")
	private Integer status; // Field 2 ( 0, 4) : INT(10)
	/**
	 * This field corresponds to the database column definition INT(10).
	 * 
	 * @Serial INT(10); This field was mapped using the type mapping INTEGER_MAPPING (java.lang.Integer, int)
	 */
	@Column(name = "company_id")
	private Integer companyId; // Field 3 ( 0, 8) : INT(10)
	/**
	 * This field corresponds to the database column definition INT(10).
	 * 
	 * @Serial INT(10); This field was mapped using the type mapping INTEGER_MAPPING (java.lang.Integer, int)
	 */
	@Column(name = "city_id")
	private Integer cityId; // Field 4 ( 0, 16) : INT(10)
	/**
	 * This field corresponds to the database column definition DOUBLE(22).
	 * 
	 * @Serial DOUBLE(22); This field was mapped using the type mapping DOUBLE_MAPPING (java.lang.Double, double)
	 */
	@Column(name = "current_location_x")
	private Double currentLocationX; // Field 5 ( 0, 32) : DOUBLE(22)
	/**
	 * This field corresponds to the database column definition DOUBLE(22).
	 * 
	 * @Serial DOUBLE(22); This field was mapped using the type mapping DOUBLE_MAPPING (java.lang.Double, double)
	 */
	@Column(name = "current_location_y")
	private Double currentLocationY; // Field 6 ( 0, 64) : DOUBLE(22)
	// </editor-fold>

	// <editor-fold defaultstate="expanded" desc="*** Constructors ***">

}
