package kr.ac.chungbuk.mario.dbms;

import java.sql.Timestamp; //date

import lombok.Data;
@Data
public class Call {

	private Integer callId; // Field 0 ( 0, 1) : INT(10) NOT NULL AUTO_INCREMENT
	private Integer taxiId; // Field 1 ( 0, 2) : INT(10)
	private Integer userId; // Field 2 ( 0, 4) : INT(10)
	private Integer status; // Field 3 ( 0, 8) : INT(10)
	private Double fromLocationX; // Field 4 ( 0, 16) : GEOMETRY
	private Double fromLocationY; // Field 4 ( 0, 16) : GEOMETRY
	private Double toLocationX; // Field 5 ( 0, 32) : GEOMETRY
	private Double toLocationY; // Field 5 ( 0, 32) : GEOMETRY
	private String fromLocationCaption; // Field 6 ( 0, 64) : VARCHAR(255)
	private String toLocationCaption; // Field 7 ( 0, 128) : VARCHAR(255)
	private Timestamp date; // Field 8 ( 1, 1) : DATETIME(19)

}
