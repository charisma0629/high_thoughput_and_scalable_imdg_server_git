package kr.ac.chungbuk.mario.service;

import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;


public class CLIService {

	public boolean cli(String[] args)  {
		boolean returnValue = true;
		/* APACHE CLI */
		CommandLineParser parser = new DefaultParser();

		// create the Options
		Options options = new Options();

		options.addOption("h", "help", false, "Display help context");
		options.addOption(Option.builder("a").longOpt("author").desc("describe the maker").argName("author").required(false).build());

//		options.addOption(Option.builder("a").longOpt("author").desc("describe the maker").hasArg().argName("author").required(true).build());

		// parse the command line arguments
		CommandLine line = null;
		try {
			line = parser.parse(options, args);
		} catch (ParseException e) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("MARIO IMDG SERVER", options);
			System.exit(-1);
		}

		if (line.hasOption("help")) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("MARIO IMDG SERVER", options);
		}
		if (line.hasOption("author")) {
			System.out.println("This application was made by Mario Choi( charisma0629@gmail.com).");
		}
		
		return returnValue;

	}
	
	public  void printMarioAsciiPicture() throws IOException{
		String text=
				"  ####            ####          ###        ##########       ######       #########           \n"+
				"   ###           ####           ###          ###   ####      ####      ###      ####         \n"+
				"   ####          ####          ####          ###    ###       ##      ###         ###        \n"+
				"   ####         #####          #####         ###    ####      ##     ###          ####       \n"+
				"   #####        #####         ######         ###     ###      ##     ###           ####      \n"+
				"   #####       ######         ##  ##         ###     ###      ##    ###            ####      \n"+
				"   ######      ## ###         ##  ###        ###    ###       ##    ###             ###      \n"+
				"   ## ###     ### ###        ##   ###        ###    ###       ##    ###             ###      \n"+
				"   ##  ###    ##  ###        ##   ###        ########         ##    ###             ###      \n"+
				"   ##  ###   ###  ###       ##########       ########         ##    ###             ###      \n"+
				"   ##  ####  ##   ###       ###    ###       ###  ###         ##    ###             ###      \n"+
				"   ##   ###  ##   ###      ###      ###      ###   ###        ##    ####            ###      \n"+
				"  ###   ######    ###      ###      ###      ###   ####       ##     ###           ###       \n"+
				"  ###    #####    ###      ##       ###      ###    ####      ##     ####          ###       \n"+
				"  ###    ####     ###     ###        ###     ###     ###      ##      ####        ###        \n"+
				"  ###     ###     ####   ####        ####   ####      ####   ####      ####      ###         \n"+
				"#######         ####### #######    ###############     #### ######       #########           \n"+
				"                        :: Mario Application :: (v0.1.0.RELEASE)\n";
		System.out.println(text);
	}
	public  void printMarioAsciiPictureR2(){
		String text=
						"*********************************************************************************************\n"+
						"  ####            ####          ###        ##########       ######       #########           \n"+
						"   ###           ####           ###          ###   ####      ####      ###      ####         \n"+
						"   ####          ####          ####          ###    ###       ##      ###         ###        \n"+
						"   ####         #####          #####         ###    ####      ##     ###          ####       \n"+
						"   #####        #####         ######         ###     ###      ##     ###           ####      \n"+
						"   #####       ######         ##  ##         ###     ###      ##    ###            ####      \n"+
						"   ######      ## ###         ##  ###        ###    ###       ##    ###             ###      \n"+
						"   ## ###     ### ###        ##   ###        ###    ###       ##    ###             ###      \n"+
						"   ##  ###    ##  ###        ##   ###        ########         ##    ###             ###      \n"+
						"   ##  ###   ###  ###       ##########       ########         ##    ###             ###      \n"+
						"   ##  ####  ##   ###       ###    ###       ###  ###         ##    ###             ###      \n"+
						"   ##   ###  ##   ###      ###      ###      ###   ###        ##    ####            ###      \n"+
						"  ###   ######    ###      ###      ###      ###   ####       ##     ###           ###       \n"+
						"  ###    #####    ###      ##       ###      ###    ####      ##     ####          ###       \n"+
						"  ###    ####     ###     ###        ###     ###     ###      ##      ####        ###        \n"+
						"  ###     ###     ####   ####        ####   ####      ####   ####      ####      ###         \n"+
						"#######         ####### #######    ###############     #### ######       #########           \n"+
						"*********************************************************************************************\n"+
						"                        :: Mario Application :: (v0.1.0.RELEASE)\n"+
						"*********************************************************************************************\n";
		System.out.println(text);
	}
}
