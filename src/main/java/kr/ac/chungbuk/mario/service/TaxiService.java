package kr.ac.chungbuk.mario.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.SqlPredicate;

import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.taxi.Taxi;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.taxi.TaxiMgr;
import kr.ac.chungbuk.mario.repository.TaxiRepository;
import kr.ac.chungbuk.mario.util.Haversine;

@Service
public class TaxiService {

	@Autowired
	HazelcastInstance hazelcastInstance;

	@Autowired
	TaxiRepository taxiRepository;

	public Collection<Taxi> findTaxiAll() {

		IMap<String, Taxi> map = hazelcastInstance.getMap(TaxiMgr.getHazelcastMapName());
		return map.values();
	}

	public Collection<Taxi> findTaxiNear2KmNaive(double x, double y) {

		IMap<String, Taxi> map = hazelcastInstance.getMap(TaxiMgr.getHazelcastMapName());

		Collection<Taxi> result = map.values();

		/* CASE2 : stream */
		List<Taxi> finalResult = result.parallelStream().filter((s) -> {
			if (Haversine.haversine(y, x, s.getCurrentLocationY_Primitive(), s.getCurrentLocationX()) <= 2.0)
				return true;
			return false;
		}).collect(Collectors.toList());

		// end = Instant.now();
		// System.out.println(Duration.between(start, end)); // prints PT1M3.553S

		return finalResult;
	}

	public Collection<Taxi> findTaxiNear4KmNaive(double x, double y) {
		IMap<String, Taxi> map = hazelcastInstance.getMap(TaxiMgr.getHazelcastMapName());
		Collection<Taxi> result = map.values();

		/* CASE2 : stream */
		List<Taxi> finalResult = result.parallelStream().filter((s) -> {
			if (Haversine.haversine(y, x, s.getCurrentLocationY_Primitive(), s.getCurrentLocationX()) <= 2.0)
				return true;
			return false;
		}).collect(Collectors.toList());

		return finalResult;
	}

	public Collection<Taxi> findTaxiNear2KmSquare(double x, double y) {

		IMap<String, Taxi> map = hazelcastInstance.getMap(TaxiMgr.getHazelcastMapName());
		BigDecimal latitudeAlpha = new BigDecimal(0.019145); // change to proper value
		BigDecimal longitudeAlpha = new BigDecimal(0.02326); // change to proper value
		BigDecimal latitudeD = BigDecimal.valueOf(Double.valueOf(y));
		BigDecimal longitudeD = BigDecimal.valueOf(Double.valueOf(x));
		Collection<Taxi> result = map.values(new SqlPredicate("currentLocationY BETWEEN " + latitudeD.subtract(latitudeAlpha) + " AND " + latitudeD.add(latitudeAlpha) + " AND currentLocationX BETWEEN " + longitudeD.subtract(longitudeAlpha) + " AND " + longitudeD.add(longitudeAlpha)));
		return result;
	}

	public Collection<Taxi> findTaxiNear4KmSquare(double x, double y) {
		IMap<String, Taxi> map = hazelcastInstance.getMap(TaxiMgr.getHazelcastMapName());
		BigDecimal latitudeAlpha = new BigDecimal(0.38290); // change to proper value
		BigDecimal longitudeAlpha = new BigDecimal(0.4652); // change to proper value
		BigDecimal latitudeD = BigDecimal.valueOf(Double.valueOf(y));
		BigDecimal longitudeD = BigDecimal.valueOf(Double.valueOf(x));

		Collection<Taxi> result = map.values(new SqlPredicate("currentLocationY BETWEEN " + latitudeD.subtract(latitudeAlpha) + " AND " + latitudeD.add(latitudeAlpha) + " AND currentLocationX BETWEEN " + longitudeD.subtract(longitudeAlpha) + " AND " + longitudeD.add(longitudeAlpha)));

		return result;
	}

	public Collection<Taxi> findTaxiNear2KmSquareGCD(double x, double y) {

		IMap<String, Taxi> map = hazelcastInstance.getMap(TaxiMgr.getHazelcastMapName());
		BigDecimal latitudeAlpha = new BigDecimal(0.019145); // change to proper value
		BigDecimal longitudeAlpha = new BigDecimal(0.02326); // change to proper value
		BigDecimal latitudeD = BigDecimal.valueOf(Double.valueOf(y));
		BigDecimal longitudeD = BigDecimal.valueOf(Double.valueOf(x));

		Collection<Taxi> result = map.values(new SqlPredicate("currentLocationY BETWEEN " + latitudeD.subtract(latitudeAlpha) + " AND " + latitudeD.add(latitudeAlpha) + " AND currentLocationX BETWEEN " + longitudeD.subtract(longitudeAlpha) + " AND " + longitudeD.add(longitudeAlpha)));
		List<Taxi> finalResult = result.parallelStream().filter((s) -> {
			if (Haversine.haversine(y, x, s.getCurrentLocationY_Primitive(), s.getCurrentLocationX()) <= 2.0)
				return true;
			return false;
		}).collect(Collectors.toList());

		return finalResult;
	}

	public Collection<Taxi> findTaxiNear4KmSquareGCD(double x, double y) {

		IMap<String, Taxi> map = hazelcastInstance.getMap(TaxiMgr.getHazelcastMapName());
		BigDecimal latitudeAlpha = new BigDecimal(0.38290); // change to proper value
		BigDecimal longitudeAlpha = new BigDecimal(0.4652); // change to proper value
		BigDecimal latitudeD = BigDecimal.valueOf(Double.valueOf(y));
		BigDecimal longitudeD = BigDecimal.valueOf(Double.valueOf(x));

		Collection<Taxi> result = map.values(new SqlPredicate("currentLocationY BETWEEN " + latitudeD.subtract(latitudeAlpha) + " AND " + latitudeD.add(latitudeAlpha) + " AND currentLocationX BETWEEN " + longitudeD.subtract(longitudeAlpha) + " AND " + longitudeD.add(longitudeAlpha)));
		List<Taxi> finalResult = result.parallelStream().filter((s) -> {
			if (Haversine.haversine(y, x, s.getCurrentLocationY_Primitive(), s.getCurrentLocationX()) <= 2.0)
				return true;
			return false;
		}).collect(Collectors.toList());

		return finalResult;
	}

	public List<kr.ac.chungbuk.mario.dbms.Taxi> findTaxiAllFromDB() {
		return taxiRepository.findAll();
	}

	public List<kr.ac.chungbuk.mario.dbms.Taxi> findTaxiNear2KmNaiveFromDB(double x, double y) {
		return taxiRepository.findTaxiNear2KmNaive(x, y);
	}

	public List<kr.ac.chungbuk.mario.dbms.Taxi> findTaxiNear2KmSquareFromDB(double x, double y) {
		BigDecimal latitudeAlpha = new BigDecimal(0.019145); // change to proper value
		BigDecimal longitudeAlpha = new BigDecimal(0.02326); // change to proper value
		BigDecimal latitudeD = BigDecimal.valueOf(Double.valueOf(y));
		BigDecimal longitudeD = BigDecimal.valueOf(Double.valueOf(x));

		return taxiRepository.findTaxiNear2KmSquare(longitudeD.subtract(longitudeAlpha).doubleValue(), latitudeD.subtract(latitudeAlpha).doubleValue(), longitudeD.add(longitudeAlpha).doubleValue(), latitudeD.add(latitudeAlpha).doubleValue());
	}

	public List<kr.ac.chungbuk.mario.dbms.Taxi> findTaxiNear2KmSquareGCDFromDB(double x, double y) {
		BigDecimal latitudeAlpha = new BigDecimal(0.019145); // change to proper value
		BigDecimal longitudeAlpha = new BigDecimal(0.02326); // change to proper value
		BigDecimal latitudeD = BigDecimal.valueOf(Double.valueOf(y));
		BigDecimal longitudeD = BigDecimal.valueOf(Double.valueOf(x));

		return taxiRepository.findTaxiNear2KmSquareGCD(longitudeD.subtract(longitudeAlpha).doubleValue(), latitudeD.subtract(latitudeAlpha).doubleValue(), longitudeD.add(longitudeAlpha).doubleValue(), latitudeD.add(latitudeAlpha).doubleValue(),x,y);
	}
}
