package kr.ac.chungbuk.mario.util;

import org.apache.log4j.Logger;

public class StopWatch {
	static Logger logger = Logger.getLogger(StopWatch.class);

	public static void capture(String name, long start, long end) {
		logger.debug(name + "\t" + (end - start) + " ms");
	}
}
