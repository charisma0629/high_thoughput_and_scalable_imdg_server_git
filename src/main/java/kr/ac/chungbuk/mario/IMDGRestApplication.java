package kr.ac.chungbuk.mario;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.taxi.Taxi;
import kr.ac.chungbuk.mario.service.TaxiService;

/**
 * @author charisma0629
 *
 */
@ComponentScan
@RestController
@RequestMapping("api/taxi")
public class IMDGRestApplication {

	@Autowired
	TaxiService taxiService;

	/**
	 * IMDG FIND ALL TAXIS<br>
	 * curl -X GET "http://localhost:8080/api/taxi/imdg"
	 * 
	 * @return
	 */
	@RequestMapping(value = "imdg", method = RequestMethod.GET)
	Collection<Taxi> getTaxiAll() {
		return taxiService.findTaxiAll();
	}

	/**
	 * IMDG SEARCH CASE 1<br/>
	 * curl -X GET "http://localhost:8080/api/taxi/imdg/near2km_naive?x=1.0&y=2.0"
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	@RequestMapping(value = "imdg/near2km_naive", method = RequestMethod.GET)
	Collection<Taxi> getTaxiNear2KmPrecisely(@RequestParam(value = "x") double x, @RequestParam(value = "y") double y) {
		return taxiService.findTaxiNear2KmNaive(x, y);
	}

	/**
	 * IMDG SEARCH CASE 1<br/>
	 * curl -X GET "http://localhost:8080/api/taxi/imdg/near4km_naive?x=1.0&y=2.0"
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	@RequestMapping(value = "imdg/near4km_naive", method = RequestMethod.GET)
	Collection<Taxi> getTaxiNear4KmPrecisely(@RequestParam(value = "x") double x, @RequestParam(value = "y") double y) {
		return taxiService.findTaxiNear4KmNaive(x, y);
	}

	/**
	 * IMDG SEARCH CASE 2<br/>
	 * curl -X GET "http://localhost:8080/api/taxi/imdg/near2km_square?x=1.0&y=2.0"
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	@RequestMapping(value = "imdg/near2km_square", method = RequestMethod.GET)
	Collection<Taxi> getTaxiNear2Km(@RequestParam(value = "x") double x, @RequestParam(value = "y") double y) {
		return taxiService.findTaxiNear2KmSquare(x, y);
	}

	/**
	 * IMDG SEARCH CASE 3(2->1)<br/>
	 * curl -X GET "http://localhost:8080/api/taxi/imdg/near2km_square_gcd?x=1.0&y=2.0"
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	@RequestMapping(value = "imdg/near2km_square_gcd", method = RequestMethod.GET)
	Collection<Taxi> getTaxiNear2KmSquareGCD(@RequestParam(value = "x") double x, @RequestParam(value = "y") double y) {
		return taxiService.findTaxiNear2KmSquareGCD(x, y);
	}

	/**
	 * IMDG SEARCH CASE 2<br/>
	 * curl -X GET "http://localhost:8080/api/taxi/imdg/near4km_square?x=1.0&y=2.0"
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	@RequestMapping(value = "imdg/near4km_square", method = RequestMethod.GET)
	Collection<Taxi> getTaxiNear4KmSquare(@RequestParam(value = "x") double x, @RequestParam(value = "y") double y) {
		return taxiService.findTaxiNear4KmSquare(x, y);
	}

	/**
	 * IMDG SEARCH CASE 3<br/>
	 * curl -X GET "http://localhost:8080/api/taxi/imdg/near4km_square_gcd?x=1.0&y=2.0"
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	@RequestMapping(value = "imdg/near4km_square_gcd", method = RequestMethod.GET)
	Collection<Taxi> getTaxiNear4KmSquareGCD(@RequestParam(value = "x") double x, @RequestParam(value = "y") double y) {
		return taxiService.findTaxiNear4KmSquareGCD(x, y);
	}

	/**
	 * DBMS FIND ALL TAXIS <br/>
	 * curl -X GET "http://localhost:8080/api/taxi/db"
	 * 
	 * @return
	 */
	@RequestMapping(value = "db", method = RequestMethod.GET)
	List<kr.ac.chungbuk.mario.dbms.Taxi> getTaxiAllFromDB() {
		return taxiService.findTaxiAllFromDB();
	}

	/**
	 * DBMS SEARCH CASE 1<br/>
	 * curl -X GET "http://localhost:8080/api/taxi/db/near2km_naive?x=127.0&y=36.0"
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	@RequestMapping(value = "db/near2km_naive", method = RequestMethod.GET)
	List<kr.ac.chungbuk.mario.dbms.Taxi> getTaxiNear2KmNaiveFromDB(@RequestParam(value = "x") double x, @RequestParam(value = "y") double y) {
		return taxiService.findTaxiNear2KmNaiveFromDB(x, y);
	}

	/**
	 * DBMS SEARCH CASE 2<br/>
	 * curl -X GET "http://localhost:8080/api/taxi/db/near2km_square?x=127.0&y=36.0"
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	@RequestMapping(value = "db/near2km_square", method = RequestMethod.GET)
	List<kr.ac.chungbuk.mario.dbms.Taxi> getTaxiNear2KmSquare(@RequestParam(value = "x") double x, @RequestParam(value = "y") double y) {
		return taxiService.findTaxiNear2KmSquareFromDB(x, y);
	}

	/**
	 * DBMS SEARCH CASE 3(2->1)<br/>
	 * curl -X GET "http://localhost:8080/api/taxi/db/near2km_square_gcd?x=127.0&y=36.0"
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	@RequestMapping(value = "db/near2km_square_gcd", method = RequestMethod.GET)
	List<kr.ac.chungbuk.mario.dbms.Taxi> getTaxiNear2KmSquareGCDFromDB(@RequestParam(value = "x") double x, @RequestParam(value = "y") double y) {
		return taxiService.findTaxiNear2KmSquareGCDFromDB(x, y);
	}

}
