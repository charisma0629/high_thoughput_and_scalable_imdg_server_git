package kr.ac.chungbuk.mario;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@ConfigurationProperties(prefix = "imdg")
@NoArgsConstructor
public @Data class ImdgProperties {
	private String name;
	private String password;

	private List<String> imdg_server = new ArrayList<String>();

	private String ip_range;

	private String port;

}
