package kr.ac.chungbuk.mario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import kr.ac.chungbuk.mario.service.CLIService;


@SpringBootApplication
@ComponentScan
@EnableAutoConfiguration
public class ServerDriver {

//	@Autowired
//	CLIService cLIService;
//
//	@Override
//	public void run(String... args) throws Exception {
//		cLIService.printMarioAsciiPictureR2();
//		cLIService.cli(args);
//	}

	public static void main(String[] args) throws Exception {
		CLIService cLIService = new CLIService();
		cLIService.printMarioAsciiPictureR2();
		cLIService.cli(args);
		
		SpringApplication.run(ServerDriver.class, args);

	}

}
