package kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.company;

//<editor-fold defaultstate="collapsed" desc="*** Imports ***">
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.company.base.CompanyMgrBase;
//</editor-fold>

/**
 * @author Generated by www.speedment.com
 * 
 * This class can be used for overriding methods in the {@link kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.company.CompanyMgrBase CompanyMgrBase} class.
 */
public final class CompanyMgr extends CompanyMgrBase {
    //<editor-fold defaultstate="collapsed" desc="*** Singleton Methods ***">
    /**
     * Gets the singleton instance for the {@link kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.company.CompanyMgr CompanyMgr}
     * 
     * @return the singleton instance for the {@link kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.company.CompanyMgr CompanyMgr}
     */
    public static CompanyMgr getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        private static final CompanyMgr INSTANCE = new CompanyMgr();
    }

    //</editor-fold>
    
    /**
     * Plain constructor
     */
    private CompanyMgr() {
        super();
    }

}

