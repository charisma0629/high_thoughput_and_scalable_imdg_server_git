package kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.taxi.base;

//<editor-fold defaultstate="collapsed" desc="*** Imports ***">
import com.speedment.core.cache.common.StructContainer;
import com.speedment.core.cache.list.OrderByType;
import com.speedment.core.cache.list.StructArrayList;
import com.speedment.core.cache.list.StructList;
import com.speedment.core.cache.map.StructMap;
import com.speedment.core.db.model.Column;
import com.speedment.core.expression.WhereExpression;
import com.speedment.core.tableorder.TableOrder;
import java.util.Comparator;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.call.Call;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.taxi.Taxi;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.taxi.TaxiList;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.taxi.TaxiMap;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.taxi.TaxiMapOnHeapImpl;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.taxi.base.TaxiBase;
//</editor-fold>

/**
 * @author Generated by www.speedment.com
 * 
 * This file is generated automatically using the Speedment Java 1.7 plugin.
 * Any manual changes may be overwritten.
 * Please make changes by overriding methods in the {@link kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.taxi.TaxiTaxiMap TaxiTaxiMap} class.
 */
public class TaxiListBase extends StructArrayList<Taxi> {
    
    private static final long serialVersionUID = TaxiBase.SERIAL_VERSION_UID - 5;
    
    /**
     * Plain constructor
     */
    public TaxiListBase() {
        super();
    }

    /**
     * Copies a new (shallow) instance of this <code>Taxi</code> object.
     */
    public TaxiListBase(final TaxiListBase taxiList) {
        super(taxiList);
    }

    //Instance methods
    @Override 
    public TaxiList newInstance() {
        return new TaxiList();
    }

    @Override 
    public StructMap<String, Taxi> newMapInstance() {
        throw new UnsupportedOperationException("Unable to create generic StructMap<K, V> from a StructList<V>, K unknown");
    }

    @Override 
    public TaxiMap newStringMapInstance() {
        return new TaxiMapOnHeapImpl();
    }

    @Override 
    public TaxiList newListInstance() {
        return new TaxiList();
    }

    //Override methods
    public TaxiList orderByCall(final TableOrder<Call> order) {
        return TaxiMapOnHeapImplBase.orderByCall(this, order, OrderByType.ASC);
    }

    public TaxiList orderByCall(final TableOrder<Call> order, final OrderByType type) {
        return TaxiMapOnHeapImplBase.orderByCall(this, order, type);
    }

    @Override 
    public TaxiList clone() {
        return (TaxiList)super.clone();
    }
    
    @Override 
    public TaxiList limit(final long limit) {
        return (TaxiList)super.limit(limit);
    }
    
    @Override 
    public TaxiList limit(final long start, final long limit) {
        return (TaxiList)super.limit(start, limit);
    }
    
    @Override 
    public TaxiList orderBy(final Column... columns) {
        return (TaxiList)super.orderBy(columns);
    }
    
    @Override 
    public TaxiList orderBy(final String... expressions) {
        return (TaxiList)super.orderBy(expressions);
    }
    
    @Override 
    public TaxiList orderBy(final Comparator<Taxi> comparator) {
        return (TaxiList)super.orderBy(comparator);
    }
    
    @Override 
    public TaxiList orderBy(final Comparator<Taxi> comparator, final OrderByType orderByType) {
        return (TaxiList)super.orderBy(comparator, orderByType);
    }
    
    @Override 
    public TaxiList orderBy(final Column column, final OrderByType orderByType) {
        return (TaxiList)super.orderBy(column, orderByType);
    }
    
    @Override 
    public TaxiList orderBy(final OrderByType orderByType, final Column... columns) {
        return (TaxiList)super.orderBy(orderByType, columns);
    }
    
    @Override 
    public TaxiList orderBy(final OrderByType orderByType, final String... expressions) {
        return (TaxiList)super.orderBy(orderByType, expressions);
    }
    
    @Override 
    public TaxiList orderBy(final Comparator<Taxi> comparator0, final Comparator<Taxi> comparator1) {
        return (TaxiList)super.orderBy(comparator0, comparator1);
    }
    
    @Override 
    public TaxiList orderBy(final Comparator<Taxi> comparator0, final Comparator<Taxi> comparator1, final Comparator<Taxi> comparator2) {
        return (TaxiList)super.orderBy(comparator0, comparator1, comparator2);
    }
    
    @Override 
    public TaxiList orderBy(final Comparator<Taxi> comparator0, final OrderByType orderByType1, final Comparator<Taxi> comparator2, final OrderByType orderByType3) {
        return (TaxiList)super.orderBy(comparator0, orderByType1, comparator2, orderByType3);
    }
    
    @Override 
    public TaxiList orderBy(final Column column0, final OrderByType orderByType1, final Column column2, final OrderByType orderByType3) {
        return (TaxiList)super.orderBy(column0, orderByType1, column2, orderByType3);
    }
    
    @Override 
    public TaxiList orderBy(final Comparator<Taxi> comparator0, final OrderByType orderByType1, final Comparator<Taxi> comparator2, final OrderByType orderByType3, final Comparator<Taxi> comparator4, final OrderByType orderByType5) {
        return (TaxiList)super.orderBy(comparator0, orderByType1, comparator2, orderByType3, comparator4, orderByType5);
    }
    
    @Override 
    public TaxiList orderBy(final Column column0, final OrderByType orderByType1, final Column column2, final OrderByType orderByType3, final Column column4, final OrderByType orderByType5) {
        return (TaxiList)super.orderBy(column0, orderByType1, column2, orderByType3, column4, orderByType5);
    }
    
    @Override 
    public TaxiList orderByPrimaryKey() {
        return (TaxiList)super.orderByPrimaryKey();
    }
    
    @Override 
    public TaxiList toList() {
        return (TaxiList)super.toList();
    }
    
    @Override 
    public TaxiMap toMap() {
        return (TaxiMap)super.toMap();
    }
    
    @Override 
    public TaxiList toSortedList() {
        return (TaxiList)super.toSortedList();
    }
    
    @Override 
    public TaxiList union(final StructContainer<String, Taxi> structContainer) {
        return (TaxiList)super.union(structContainer);
    }
    
    @Override 
    public TaxiList where(final WhereExpression<Taxi> whereExpression) {
        return (TaxiList)super.where(whereExpression);
    }
    
    @Override 
    public TaxiList where(final String expression) {
        return (TaxiList)super.where(expression);
    }
    
    @Override 
    public TaxiList where(final WhereExpression<Taxi> whereExpression0, final WhereExpression<Taxi> whereExpression1) {
        return (TaxiList)super.where(whereExpression0, whereExpression1);
    }
    
    @Override 
    public TaxiList where(final WhereExpression<Taxi> whereExpression0, final WhereExpression<Taxi> whereExpression1, final WhereExpression<Taxi> whereExpression2) {
        return (TaxiList)super.where(whereExpression0, whereExpression1, whereExpression2);
    }
    
}

