/* No trigger is created for the `mario_taxi`.`TRIGGERLOG` table itself. */

DROP TRIGGER IF EXISTS `mario_taxi`.`call_i`;
delimiter /// 
    CREATE TRIGGER `mario_taxi`.`call_i` AFTER insert ON `mario_taxi`.`call` 
        FOR EACH ROW BEGIN 
            INSERT INTO `mario_taxi`.`TRIGGERLOG` (`CTIME`, `TYPE_CODE`, `SCHEMA_NAME`, `TABLE_NAME`, `NEW_PRIMARYKEY0`, `CLIENT_NAME`) VALUES (now(), 1, 'mario_taxi', 'call', NEW.`call_id`, user()); 
        END;        /// 
delimiter ;

DROP TRIGGER IF EXISTS `mario_taxi`.`call_u`;
delimiter /// 
    CREATE TRIGGER `mario_taxi`.`call_u` AFTER update ON `mario_taxi`.`call` 
        FOR EACH ROW BEGIN 
            INSERT INTO `mario_taxi`.`TRIGGERLOG` (`CTIME`, `TYPE_CODE`, `SCHEMA_NAME`, `TABLE_NAME`, `OLD_PRIMARYKEY0`, `NEW_PRIMARYKEY0`, `CLIENT_NAME`) VALUES (now(), 2, 'mario_taxi', 'call', OLD.`call_id`, NEW.`call_id`, user()); 
        END;        /// 
delimiter ;

DROP TRIGGER IF EXISTS `mario_taxi`.`call_d`;
delimiter /// 
    CREATE TRIGGER `mario_taxi`.`call_d` AFTER delete ON `mario_taxi`.`call` 
        FOR EACH ROW BEGIN 
            INSERT INTO `mario_taxi`.`TRIGGERLOG` (`CTIME`, `TYPE_CODE`, `SCHEMA_NAME`, `TABLE_NAME`, `OLD_PRIMARYKEY0`, `CLIENT_NAME`) VALUES (now(), 3, 'mario_taxi', 'call', OLD.`call_id`, user()); 
        END;        /// 
delimiter ;


DROP TRIGGER IF EXISTS `mario_taxi`.`city_i`;
delimiter /// 
    CREATE TRIGGER `mario_taxi`.`city_i` AFTER insert ON `mario_taxi`.`city` 
        FOR EACH ROW BEGIN 
            INSERT INTO `mario_taxi`.`TRIGGERLOG` (`CTIME`, `TYPE_CODE`, `SCHEMA_NAME`, `TABLE_NAME`, `NEW_PRIMARYKEY0`, `CLIENT_NAME`) VALUES (now(), 1, 'mario_taxi', 'city', NEW.`city_id`, user()); 
        END;        /// 
delimiter ;

DROP TRIGGER IF EXISTS `mario_taxi`.`city_u`;
delimiter /// 
    CREATE TRIGGER `mario_taxi`.`city_u` AFTER update ON `mario_taxi`.`city` 
        FOR EACH ROW BEGIN 
            INSERT INTO `mario_taxi`.`TRIGGERLOG` (`CTIME`, `TYPE_CODE`, `SCHEMA_NAME`, `TABLE_NAME`, `OLD_PRIMARYKEY0`, `NEW_PRIMARYKEY0`, `CLIENT_NAME`) VALUES (now(), 2, 'mario_taxi', 'city', OLD.`city_id`, NEW.`city_id`, user()); 
        END;        /// 
delimiter ;

DROP TRIGGER IF EXISTS `mario_taxi`.`city_d`;
delimiter /// 
    CREATE TRIGGER `mario_taxi`.`city_d` AFTER delete ON `mario_taxi`.`city` 
        FOR EACH ROW BEGIN 
            INSERT INTO `mario_taxi`.`TRIGGERLOG` (`CTIME`, `TYPE_CODE`, `SCHEMA_NAME`, `TABLE_NAME`, `OLD_PRIMARYKEY0`, `CLIENT_NAME`) VALUES (now(), 3, 'mario_taxi', 'city', OLD.`city_id`, user()); 
        END;        /// 
delimiter ;


DROP TRIGGER IF EXISTS `mario_taxi`.`company_i`;
delimiter /// 
    CREATE TRIGGER `mario_taxi`.`company_i` AFTER insert ON `mario_taxi`.`company` 
        FOR EACH ROW BEGIN 
            INSERT INTO `mario_taxi`.`TRIGGERLOG` (`CTIME`, `TYPE_CODE`, `SCHEMA_NAME`, `TABLE_NAME`, `NEW_PRIMARYKEY0`, `CLIENT_NAME`) VALUES (now(), 1, 'mario_taxi', 'company', NEW.`company_id`, user()); 
        END;        /// 
delimiter ;

DROP TRIGGER IF EXISTS `mario_taxi`.`company_u`;
delimiter /// 
    CREATE TRIGGER `mario_taxi`.`company_u` AFTER update ON `mario_taxi`.`company` 
        FOR EACH ROW BEGIN 
            INSERT INTO `mario_taxi`.`TRIGGERLOG` (`CTIME`, `TYPE_CODE`, `SCHEMA_NAME`, `TABLE_NAME`, `OLD_PRIMARYKEY0`, `NEW_PRIMARYKEY0`, `CLIENT_NAME`) VALUES (now(), 2, 'mario_taxi', 'company', OLD.`company_id`, NEW.`company_id`, user()); 
        END;        /// 
delimiter ;

DROP TRIGGER IF EXISTS `mario_taxi`.`company_d`;
delimiter /// 
    CREATE TRIGGER `mario_taxi`.`company_d` AFTER delete ON `mario_taxi`.`company` 
        FOR EACH ROW BEGIN 
            INSERT INTO `mario_taxi`.`TRIGGERLOG` (`CTIME`, `TYPE_CODE`, `SCHEMA_NAME`, `TABLE_NAME`, `OLD_PRIMARYKEY0`, `CLIENT_NAME`) VALUES (now(), 3, 'mario_taxi', 'company', OLD.`company_id`, user()); 
        END;        /// 
delimiter ;


DROP TRIGGER IF EXISTS `mario_taxi`.`taxi_i`;
delimiter /// 
    CREATE TRIGGER `mario_taxi`.`taxi_i` AFTER insert ON `mario_taxi`.`taxi` 
        FOR EACH ROW BEGIN 
            INSERT INTO `mario_taxi`.`TRIGGERLOG` (`CTIME`, `TYPE_CODE`, `SCHEMA_NAME`, `TABLE_NAME`, `NEW_PRIMARYKEY0`, `CLIENT_NAME`) VALUES (now(), 1, 'mario_taxi', 'taxi', NEW.`taxi_id`, user()); 
        END;        /// 
delimiter ;

DROP TRIGGER IF EXISTS `mario_taxi`.`taxi_u`;
delimiter /// 
    CREATE TRIGGER `mario_taxi`.`taxi_u` AFTER update ON `mario_taxi`.`taxi` 
        FOR EACH ROW BEGIN 
            INSERT INTO `mario_taxi`.`TRIGGERLOG` (`CTIME`, `TYPE_CODE`, `SCHEMA_NAME`, `TABLE_NAME`, `OLD_PRIMARYKEY0`, `NEW_PRIMARYKEY0`, `CLIENT_NAME`) VALUES (now(), 2, 'mario_taxi', 'taxi', OLD.`taxi_id`, NEW.`taxi_id`, user()); 
        END;        /// 
delimiter ;

DROP TRIGGER IF EXISTS `mario_taxi`.`taxi_d`;
delimiter /// 
    CREATE TRIGGER `mario_taxi`.`taxi_d` AFTER delete ON `mario_taxi`.`taxi` 
        FOR EACH ROW BEGIN 
            INSERT INTO `mario_taxi`.`TRIGGERLOG` (`CTIME`, `TYPE_CODE`, `SCHEMA_NAME`, `TABLE_NAME`, `OLD_PRIMARYKEY0`, `CLIENT_NAME`) VALUES (now(), 3, 'mario_taxi', 'taxi', OLD.`taxi_id`, user()); 
        END;        /// 
delimiter ;


DROP TRIGGER IF EXISTS `mario_taxi`.`user_i`;
delimiter /// 
    CREATE TRIGGER `mario_taxi`.`user_i` AFTER insert ON `mario_taxi`.`user` 
        FOR EACH ROW BEGIN 
            INSERT INTO `mario_taxi`.`TRIGGERLOG` (`CTIME`, `TYPE_CODE`, `SCHEMA_NAME`, `TABLE_NAME`, `NEW_PRIMARYKEY0`, `CLIENT_NAME`) VALUES (now(), 1, 'mario_taxi', 'user', NEW.`user_id`, user()); 
        END;        /// 
delimiter ;

DROP TRIGGER IF EXISTS `mario_taxi`.`user_u`;
delimiter /// 
    CREATE TRIGGER `mario_taxi`.`user_u` AFTER update ON `mario_taxi`.`user` 
        FOR EACH ROW BEGIN 
            INSERT INTO `mario_taxi`.`TRIGGERLOG` (`CTIME`, `TYPE_CODE`, `SCHEMA_NAME`, `TABLE_NAME`, `OLD_PRIMARYKEY0`, `NEW_PRIMARYKEY0`, `CLIENT_NAME`) VALUES (now(), 2, 'mario_taxi', 'user', OLD.`user_id`, NEW.`user_id`, user()); 
        END;        /// 
delimiter ;

DROP TRIGGER IF EXISTS `mario_taxi`.`user_d`;
delimiter /// 
    CREATE TRIGGER `mario_taxi`.`user_d` AFTER delete ON `mario_taxi`.`user` 
        FOR EACH ROW BEGIN 
            INSERT INTO `mario_taxi`.`TRIGGERLOG` (`CTIME`, `TYPE_CODE`, `SCHEMA_NAME`, `TABLE_NAME`, `OLD_PRIMARYKEY0`, `CLIENT_NAME`) VALUES (now(), 3, 'mario_taxi', 'user', OLD.`user_id`, user()); 
        END;        /// 
delimiter ;





