CREATE TABLE `mario_taxi`.`TRIGGERLOG` (
    `ID` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `CTIME` TIMESTAMP NOT NULL DEFAULT '1972-01-01 00:00:00' COMMENT 'Timestamp when the trigger took place',
    `TYPE_CODE` TINYINT(4) NOT NULL COMMENT 'Type of update',
    `SCHEMA_NAME` VARCHAR(255) NOT NULL COMMENT 'The name of the affected schema',
    `TABLE_NAME` VARCHAR(255) NOT NULL COMMENT 'The name of the affected table',
    `OLD_PRIMARYKEY0` VARCHAR(255) DEFAULT NULL COMMENT 'Old primary key column 0',
    `OLD_PRIMARYKEY1` VARCHAR(255) DEFAULT NULL COMMENT 'Old primary key column 1',
    `OLD_PRIMARYKEY2` VARCHAR(255) DEFAULT NULL COMMENT 'Old primary key column 2',
    `OLD_PRIMARYKEY3` VARCHAR(255) DEFAULT NULL COMMENT 'Old primary key column 3',
    `OLD_PRIMARYKEY4` VARCHAR(255) DEFAULT NULL COMMENT 'Old primary key column 4',
    `OLD_PRIMARYKEY5` VARCHAR(255) DEFAULT NULL COMMENT 'Old primary key column 5',
    `NEW_PRIMARYKEY0` VARCHAR(255) DEFAULT NULL COMMENT 'New primary key column 0',
    `NEW_PRIMARYKEY1` VARCHAR(255) DEFAULT NULL COMMENT 'New primary key column 1',
    `NEW_PRIMARYKEY2` VARCHAR(255) DEFAULT NULL COMMENT 'New primary key column 2',
    `NEW_PRIMARYKEY3` VARCHAR(255) DEFAULT NULL COMMENT 'New primary key column 3',
    `NEW_PRIMARYKEY4` VARCHAR(255) DEFAULT NULL COMMENT 'New primary key column 4',
    `NEW_PRIMARYKEY5` VARCHAR(255) DEFAULT NULL COMMENT 'New primary key column 5',
    `CLIENT_NAME` VARCHAR(255) NOT NULL COMMENT 'Name of the client that initiated the trigger event',
    PRIMARY KEY (`ID`),
    KEY `index_schema` (`SCHEMA_NAME`),
    KEY `index_table` (`TABLE_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='sg.exclude;';



