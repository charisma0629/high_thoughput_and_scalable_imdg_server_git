package kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.user.base;

//<editor-fold defaultstate="collapsed" desc="*** Imports ***">
import com.speedment.core.cache.common.StructContainer;
import com.speedment.core.cache.list.OrderByType;
import com.speedment.core.cache.list.StructArrayList;
import com.speedment.core.cache.list.StructList;
import com.speedment.core.cache.map.StructMap;
import com.speedment.core.cache.map.StructOffHeapReferencedConcurrentHashMap;
import com.speedment.core.cache.map.WriteThroughStructMap;
import com.speedment.core.db.model.Column;
import com.speedment.core.expression.WhereExpression;
import com.speedment.core.offheap.container.index.Index;
import com.speedment.core.offheap.container.trunk.Trunk;
import com.speedment.core.tableorder.TableOrder;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.call.Call;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.user.User;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.user.UserList;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.user.UserMap;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.user.UserMapOffHeapImpl;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.user.UserMapOnHeapImpl;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.user.base.UserBase;
//</editor-fold>

/**
 * @author Generated by www.speedment.com
 * 
 * This file is generated automatically using the Speedment Java 1.7 plugin.
 * Any manual changes may be overwritten.
 * Please make changes by overriding methods in the {@link kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.user.UserUserMap UserUserMap} class.
 */
public class UserMapOffHeapImplBase extends StructOffHeapReferencedConcurrentHashMap<String, User> implements UserMap, WriteThroughStructMap<String, User> {
    
    private static final long serialVersionUID = UserBase.SERIAL_VERSION_UID - 3;
    
    /**
     * Plain constructor
     */
    public UserMapOffHeapImplBase() {
        super(null, 1);
        throw new UnsupportedOperationException("This operation is not supported for Off Heap tables");
    }

    /**
     * Constructs a map with the given size
     * @param size The size
     */
    public UserMapOffHeapImplBase(int size) {
        super(null, 1);
        throw new UnsupportedOperationException("This operation is not supported for Off Heap tables");
    }

    /**
     * Copies a new (shallow) instance of this <code>User</code> object.
     */
    public UserMapOffHeapImplBase(final UserMapOffHeapImplBase userMap) {
        super(null);
        throw new UnsupportedOperationException("This operation is not supported for Off Heap tables");
    }

    /**
     * OffHeap plain constructor
     */
    public UserMapOffHeapImplBase(final Trunk<User> trunk) {
        super(trunk);
        setKeyClass(String.class);
    }

    /**
     * Constructs an OffHeap map with the given size
     */
    public UserMapOffHeapImplBase(final Trunk<User> trunk, int size) {
        super(trunk, size);
        setKeyClass(String.class);
    }

    /**
     * Constructs an OffHeap map with an index
     */
    public UserMapOffHeapImplBase(final Index<String> keyIndex, final Trunk<User> trunk) {
        super(keyIndex, trunk);
        setKeyClass(String.class);
    }

    /**
     * Constructs an OffHeap map with an index and with the given size
     */
    public UserMapOffHeapImplBase(final Index<String> keyIndex, final Trunk<User> trunk, final int size) {
        super(keyIndex, trunk, size);
        setKeyClass(String.class);
    }

    //Instance methods
    @Override 
    public UserMap newInstance() {
        return new UserMapOnHeapImpl();
    }

    @Override 
    public UserMap newMapInstance() {
        return new UserMapOnHeapImpl();
    }

    @Override 
    public UserMap newMapInstanceSameEngine() {
        return new UserMapOffHeapImpl(getValueTrunk());
    }

    @Override 
    public UserMap newStringMapInstance() {
        return new UserMapOnHeapImpl();
    }

    @Override 
    public UserList newListInstance() {
        return new UserList();
    }

    //Override methods
    @Override 
    public UserList orderByCall(final TableOrder<Call> _order) {
        return orderByCall(this.values(), _order, OrderByType.ASC);
    }

    @Override 
    public UserList orderByCall(final TableOrder<Call> _order, final OrderByType _type) {
        return orderByCall(this.values(), _order, _type);
    }

    /**
     * Orders the given {@link java.util.Collection Collection} by the foreign keys pointing towards it. Each
     * item will be assigned a orderValue based on the given {@link com.speedment.core.tableorder.TableOrder TableOrder}.
     * @param _collection The {@link java.util.Collection Collection} to order.
     * @param _order The {@link com.speedment.core.cache.list.OrderByType method} of ordering.
     * @param _type If the result should be returned {@link com.speedment.core.cache.list.OrderByType#ASC ascending} or {@link com.speedment.core.cache.list.OrderByType#DESC descending}.
     * @return The ordered list.
     */
    static UserList orderByCall(final Collection<User> _collection, final TableOrder<Call> _order, final OrderByType _type) {
        final Map<Number, UserList> _map = new TreeMap<>();
        for (final User user : _collection) {
            final Number _val = _order.sortValue(user.findCall());
            UserList _list = _map.get(_val);
            if (_list == null) {
                _list = new UserList();
                _map.put(_val, _list);
            }
            _list.add(user);
        }
        final UserList _result = new UserList();
        for (final UserList _item : _map.values()) {
            _result.addAll(_item);
        }
        if (_type == OrderByType.DESC) {
            Collections.reverse(_result);
        }
        return _result;
    }

    @Override 
    public UserMap clone() {
        return (UserMap)super.clone();
    }
    
    @Override 
    public UserMap complement(final UserMap userMap) {
        return (UserMap)super.complement(userMap);
    }
    
    @Override 
    public UserMap intersection(final UserMap userMap) {
        return (UserMap)super.intersection(userMap);
    }
    
    @Override 
    public UserList limit(final long limit) {
        return (UserList)super.limit(limit);
    }
    
    @Override 
    public UserList limit(final long start, final long limit) {
        return (UserList)super.limit(start, limit);
    }
    
    @Override 
    public UserList orderBy(final Column... columns) {
        return (UserList)super.orderBy(columns);
    }
    
    @Override 
    public UserList orderBy(final String... expressions) {
        return (UserList)super.orderBy(expressions);
    }
    
    @Override 
    public UserList orderBy(final Comparator<User> comparator) {
        return (UserList)super.orderBy(comparator);
    }
    
    @Override 
    public UserList orderBy(final Comparator<User> comparator, final OrderByType orderByType) {
        return (UserList)super.orderBy(comparator, orderByType);
    }
    
    @Override 
    public UserList orderBy(final Column column, final OrderByType orderByType) {
        return (UserList)super.orderBy(column, orderByType);
    }
    
    @Override 
    public UserList orderBy(final OrderByType orderByType, final Column... columns) {
        return (UserList)super.orderBy(orderByType, columns);
    }
    
    @Override 
    public UserList orderBy(final OrderByType orderByType, final String... expressions) {
        return (UserList)super.orderBy(orderByType, expressions);
    }
    
    @Override 
    public UserList orderBy(final Comparator<User> comparator0, final Comparator<User> comparator1) {
        return (UserList)super.orderBy(comparator0, comparator1);
    }
    
    @Override 
    public UserList orderBy(final Comparator<User> comparator0, final Comparator<User> comparator1, final Comparator<User> comparator2) {
        return (UserList)super.orderBy(comparator0, comparator1, comparator2);
    }
    
    @Override 
    public UserList orderBy(final Comparator<User> comparator0, final OrderByType orderByType1, final Comparator<User> comparator2, final OrderByType orderByType3) {
        return (UserList)super.orderBy(comparator0, orderByType1, comparator2, orderByType3);
    }
    
    @Override 
    public UserList orderBy(final Column column0, final OrderByType orderByType1, final Column column2, final OrderByType orderByType3) {
        return (UserList)super.orderBy(column0, orderByType1, column2, orderByType3);
    }
    
    @Override 
    public UserList orderBy(final Comparator<User> comparator0, final OrderByType orderByType1, final Comparator<User> comparator2, final OrderByType orderByType3, final Comparator<User> comparator4, final OrderByType orderByType5) {
        return (UserList)super.orderBy(comparator0, orderByType1, comparator2, orderByType3, comparator4, orderByType5);
    }
    
    @Override 
    public UserList orderBy(final Column column0, final OrderByType orderByType1, final Column column2, final OrderByType orderByType3, final Column column4, final OrderByType orderByType5) {
        return (UserList)super.orderBy(column0, orderByType1, column2, orderByType3, column4, orderByType5);
    }
    
    @Override 
    public UserList orderByPrimaryKey() {
        return (UserList)super.orderByPrimaryKey();
    }
    
    @Override 
    public UserList toList() {
        return (UserList)super.toList();
    }
    
    @Override 
    public UserMap toMap() {
        return (UserMap)super.toMap();
    }
    
    @Override 
    public UserList toSortedList() {
        return (UserList)super.toSortedList();
    }
    
    @Override 
    public UserMap union(final StructContainer<String, User> structContainer) {
        return (UserMap)super.union(structContainer);
    }
    
    @Override 
    public UserMap where(final WhereExpression<User> whereExpression) {
        return (UserMap)super.where(whereExpression);
    }
    
    @Override 
    public UserMap where(final String expression) {
        return (UserMap)super.where(expression);
    }
    
    @Override 
    public UserMap where(final WhereExpression<User> whereExpression0, final WhereExpression<User> whereExpression1) {
        return (UserMap)super.where(whereExpression0, whereExpression1);
    }
    
    @Override 
    public UserMap where(final WhereExpression<User> whereExpression0, final WhereExpression<User> whereExpression1, final WhereExpression<User> whereExpression2) {
        return (UserMap)super.where(whereExpression0, whereExpression1, whereExpression2);
    }
    
}

