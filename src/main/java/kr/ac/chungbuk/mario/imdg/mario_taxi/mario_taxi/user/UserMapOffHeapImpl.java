package kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.user;

//<editor-fold defaultstate="collapsed" desc="*** Imports ***">
import com.speedment.core.offheap.container.index.Index;
import com.speedment.core.offheap.container.trunk.Trunk;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.user.base.UserBase;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.user.base.UserMapOffHeapImplBase;
//</editor-fold>

/**
 * @author Generated by www.speedment.com
 * 
 * This class can be used for overriding methods in the {@link kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.user.UserUserMapOffHeapImplBase UserUserMapOffHeapImplBase} class.
 */
public final class UserMapOffHeapImpl extends UserMapOffHeapImplBase {
    
    private static final long serialVersionUID = UserBase.SERIAL_VERSION_UID - 2;
    
    public UserMapOffHeapImpl() {
        super();
    }

    public UserMapOffHeapImpl(final int _size) {
        super(_size);
    }

    public UserMapOffHeapImpl(final UserMapOffHeapImpl userMap) {
        super(userMap);
    }

    public UserMapOffHeapImpl(final Trunk<User> _trunk) {
        super(_trunk);
    }

    public UserMapOffHeapImpl(final Trunk<User> _trunk, final int _size) {
        super(_trunk, _size);
    }

    public UserMapOffHeapImpl(final Index<String> _keyIndex, final Trunk<User> _trunk) {
        super(_keyIndex, _trunk);
    }

    public UserMapOffHeapImpl(final Index<String> _keyIndex, final Trunk<User> _trunk, final int _size) {
        super(_keyIndex, _trunk, _size);
    }

}

