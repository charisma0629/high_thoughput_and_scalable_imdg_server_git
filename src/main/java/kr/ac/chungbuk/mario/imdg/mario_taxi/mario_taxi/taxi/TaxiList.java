package kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.taxi;

//<editor-fold defaultstate="collapsed" desc="*** Imports ***">
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.taxi.base.TaxiBase;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.taxi.base.TaxiListBase;
//</editor-fold>

/**
 * @author Generated by www.speedment.com
 * 
 * This class can be used for overriding methods in the {@link kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.taxi.TaxiTaxiListBase TaxiTaxiListBase} class.
 */
public final class TaxiList extends TaxiListBase {
    
    private static final long serialVersionUID = TaxiBase.SERIAL_VERSION_UID - 4;
    
    /**
     * Plain constructor
     */
    public TaxiList() {
        super();
    }

    /**
     * Copies a new (shallow) instance of this <code>Taxi</code> object.
     */
    public TaxiList(final TaxiList taxiList) {
        super(taxiList);
    }

}

