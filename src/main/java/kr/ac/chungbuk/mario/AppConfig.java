package kr.ac.chungbuk.mario;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.hazelcast.config.Config;
import com.hazelcast.config.InterfacesConfig;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.MapIndexConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.TcpIpConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.speedment.core.db.model.ProjectManager;

import kr.ac.chungbuk.mario.imdg.SpeedmentHazelcastConfig;
import kr.ac.chungbuk.mario.imdg.TreeBuilder;
import kr.ac.chungbuk.mario.imdg.mario_taxi.mario_taxi.taxi.TaxiMgr;
import net.sf.log4jdbc.Log4jdbcProxyDataSource;

@Component
@Configuration
public class AppConfig {
	@Autowired
	DataSourceProperties dataSourceProperties;
	@Autowired
	ImdgProperties imdgProperties;

	DataSource dataSource;

	@Bean
	DataSource realDataSource() {
		DataSourceBuilder factory = DataSourceBuilder.create(this.dataSourceProperties.getClassLoader()).url(this.dataSourceProperties.getUrl()).username(this.dataSourceProperties.getUsername()).password(this.dataSourceProperties.getPassword());
		this.dataSource = factory.build();
		return this.dataSource;
	}

	@Bean
	@Primary
	DataSource dataSource() {
		return new Log4jdbcProxyDataSource(this.dataSource);
	}

	@SuppressWarnings("finally")
	@Bean
	HazelcastInstance hazelcastInstance(Config config) {

		HazelcastInstance instance = Hazelcast.newHazelcastInstance(config);
		ProjectManager.getInstance().putProperty(HazelcastInstance.class, instance);
		TreeBuilder tree = null;

//		 return instance ;
		try {
			tree = new TreeBuilder();
			tree.build();
			ProjectManager.getInstance().init();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return instance;
		}
		
	}

	@Bean
	Config hazelcastConfig() {
		Config config = new Config();
		// config.getSemaphoreConfig("sync").setInitialPermits(1);
		config.getGroupConfig().setName(imdgProperties.getName()).setPassword(imdgProperties.getPassword());
		NetworkConfig networkConfig = config.getNetworkConfig();
		networkConfig.setPort(Integer.parseInt(imdgProperties.getPort()));
		networkConfig.setPortAutoIncrement(false);

		JoinConfig join = networkConfig.getJoin();
		join.getMulticastConfig().setEnabled(false);

		TcpIpConfig tcpIpConfig = join.getTcpIpConfig();
		tcpIpConfig.setEnabled(true);
		for (String specificMember : imdgProperties.getImdg_server()) {
			tcpIpConfig.addMember(specificMember);
		}

		if (imdgProperties.getIp_range() != null) {
			InterfacesConfig interfaceConfig = networkConfig.getInterfaces().setEnabled(true);
			interfaceConfig.addInterface(imdgProperties.getIp_range());
		}
		config.getMapConfig(TaxiMgr.getHazelcastMapName()).addMapIndexConfig(new MapIndexConfig("currentLocationX", true)).addMapIndexConfig(new MapIndexConfig("currentLocationY", true));

		SpeedmentHazelcastConfig.addTo(config);

		return config;
	}

}
