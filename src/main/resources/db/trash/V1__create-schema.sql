DROP TABLE `call`;
DROP TABLE `taxi`;
drop table `user`;
DROP TABLE `company`;
DROP TABLE `city`;


CREATE TABLE `mario_taxi`.`user` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `user_name` VARCHAR(255) NULL,
  `user_password` VARCHAR(255) NULL,
  `current_location_x` double NULL,
  `current_location_y` double NULL,
  PRIMARY KEY (`user_id`));

CREATE TABLE `mario_taxi`.`taxi` (
  `taxi_id` INT NOT NULL AUTO_INCREMENT,
  `driver_name` VARCHAR(255) NULL,
  `status` INT NULL,
  `company_id` INT NULL,
  `city_id` INT NULL,
  `current_location_x` double NULL,
  `current_location_y` double NULL,
  PRIMARY KEY (`taxi_id`));

CREATE TABLE `mario_taxi`.`city` (
  `city_id` INT NOT NULL AUTO_INCREMENT,
  `city_name` VARCHAR(255) NULL,
  `province` VARCHAR(255) NULL,
  PRIMARY KEY (`city_id`));

CREATE TABLE `mario_taxi`.`company` (
  `company_id` INT NOT NULL AUTO_INCREMENT,
  `company_name` VARCHAR(255) NULL,
  `address` VARCHAR(255) NULL,
  PRIMARY KEY (`company_id`));

ALTER TABLE `mario_taxi`.`taxi` 
ADD INDEX `fk_taxi_company_id_idx` (`company_id` ASC);
ALTER TABLE `mario_taxi`.`taxi` 
ADD CONSTRAINT `fk_taxi_company_id`
  FOREIGN KEY (`company_id`)
  REFERENCES `mario_taxi`.`company` (`company_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_taxi_city_id`
  FOREIGN KEY (`city_id`)
  REFERENCES `mario_taxi`.`city` (`city_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;



CREATE TABLE `mario_taxi`.`call` (
  `call_id` INT NOT NULL AUTO_INCREMENT,
  `taxi_id` INT NULL,
  `user_id` INT NULL,
  `status` INT NULL,
  `from_location_x` double NULL,
  `from_location_y` double NULL,
  `to_location_x` double NULL,
  `to_location_y` double NULL,
  `from_location_caption` VARCHAR(255) NULL,
  `to_location_caption` VARCHAR(255) NULL,
  `date` DATETIME NULL,
  PRIMARY KEY (`call_id`));

ALTER TABLE `mario_taxi`.`call` 
ADD INDEX `taxi_id_idx` (`taxi_id` ASC),
ADD INDEX `fk_call_user_id_idx` (`user_id` ASC);
ALTER TABLE `mario_taxi`.`call` 
ADD CONSTRAINT `fk_call_taxi_id`
  FOREIGN KEY (`taxi_id`)
  REFERENCES `mario_taxi`.`taxi` (`taxi_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_call_user_id`
  FOREIGN KEY (`user_id`)
  REFERENCES `mario_taxi`.`user` (`user_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;




